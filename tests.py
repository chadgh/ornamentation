import os

import pytest
from ornamentation import (
    admin_action,
    debug,
    deprecated,
    skip,
    skip_if_env,
    threaded,
    timeit,
)
from ornamentation.utils import one_and_only_one


def test_timeit_basic(capsys):
    @timeit
    def slow():
        print('starting')

    slow()
    captured = capsys.readouterr()
    assert captured.out.startswith('starting\nslow((), {}) took')


def test_timeit_report_func(capsys):
    class logger:
        def __init__(self):
            self.messages = []

        def __call__(self, msg):
            self.messages.append(msg)

    log = logger()

    @timeit(report_func=log)
    def slow():
        print('starting')

    slow()
    assert len(log.messages) == 1
    assert log.messages[0].startswith('slow((), {}) took')
    captured = capsys.readouterr()
    assert captured.out == 'starting\n'
    slow()
    assert len(log.messages) == 2


def test_deprecated_basic(capsys):
    @deprecated
    def fun():
        print('fun')

    with pytest.deprecated_call():
        fun()
    captured = capsys.readouterr()
    assert captured.out.startswith('fun')


def test_debug_no_params(capsys):
    @debug
    def func():
        print('func called')

    func()
    captured = capsys.readouterr()
    assert captured.out.startswith('func called\nfunc((), {}) took')
    assert captured.out.endswith('\nfunc((), {}) returned None\n')


def test_debug_func_exception(capsys):
    @debug
    def func():
        raise Exception()

    try:
        func()
    except Exception:
        pass
    captured = capsys.readouterr()
    assert captured.out.startswith('func((), {}) took')
    assert captured.out.endswith('\nfunc((), {}) errored Exception - \n')


def test_threaded_basic():
    @threaded
    def func():
        return 1

    res = func()
    assert res.result.get() == 1


def test_admin_action_basic():
    @admin_action
    def func():
        """test"""
        return 'rtn'

    assert hasattr(func, 'short_description')
    assert func.short_description == 'test'

    assert 'rtn' == func()


def test_skip_basic():
    @skip
    def skipped_function():
        return 'rtn'

    assert skipped_function() is None


def test_skip_if_env_basic():
    @skip_if_env
    def skipped_function():
        return 'rtn'

    assert skipped_function() == 'rtn'
    os.environ['SKIP'] = 'true'
    assert skipped_function() is None


def test_skip_if_env_custom_envar():
    @skip_if_env(envar='SKIP_PLEASE')
    def skipped_function():
        return 'rtn'

    assert skipped_function() == 'rtn'
    os.environ['SKIP'] = 'true'
    assert skipped_function() == 'rtn'
    os.environ['SKIP_PLEASE'] = 'true'
    assert skipped_function() is None


def test_one_and_only_one():
    items = [False,True,False,False]
    assert one_and_only_one(items)
    items = [0,0,1,0]
    assert one_and_only_one(items)
    items = ["","","","here"]
    assert one_and_only_one(items)


def test_not_one_and_only_one():
    items = [1,2,3,4]
    assert not one_and_only_one(items)
    items = [False,False,True,True]
    assert not one_and_only_one(items)
    items = ["","a","b",""]
    assert not one_and_only_one(items)