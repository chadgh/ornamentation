from os import chdir, pardir, path

from setuptools import find_packages, setup

VERSION = '0.5.0'
PROJECT_NAME = 'ornamentation'
PYTHON_SUPPORTED_VERSIONS = ['3.6', '3.7', '3.8']

# allow setup.py to be run from any path
chdir(path.normpath(path.join(path.abspath(__file__), pardir)))

req_path = path.join(path.dirname(__file__), 'requirements.txt')
with open(req_path) as req_file:
    REQUIREMENTS = req_file.read().split()

classifiers = [
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Development Status :: 5 - Production/Stable',
    'Topic :: Software Development :: Libraries',
    'Topic :: Software Development :: Libraries :: Python Modules',
]

README = """
ornamentation
=============
A collection of, hopefully useful, decorators. See the
`repo <https://gitlab.com/chadgh/ornamentation>`_ or
`docs <https://gitlab.com/chadgh/ornamentation/-/blob/master/docs/index.md>`_
for more information.
"""

for version in PYTHON_SUPPORTED_VERSIONS:
    classifiers.append('Programming Language :: Python :: {}'.format(version))

print(find_packages)
setup(
    name=PROJECT_NAME,
    version=VERSION,
    packages=find_packages(),
    include_package_data=True,
    description='A collection of decorators.',
    long_description=README,
    url='https://gitlab.com/chadgh/ornamentation',
    author='Chad Hansen',
    author_email='chadgh@gmail.com',
    install_requires=REQUIREMENTS,
    classifiers=classifiers,
)
