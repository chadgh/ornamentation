# `ornamentation`

A collection of decorators.

## Installation and usage

Install with `pip install ornamentation`.

Use by importing the desired decorator from ornamentation:

```python
from ornamentation import debug

@debug
def my_func():
   return
```

See what decorators are provided and what they do below.

## Debugging decorators

### `debug`

Emits a debugging message when the function is called.

**Arguments**

* `report_func` (default `print`) - Callable that the debugging message will be sent to. This could be a logging function (i.e. `logger.error`). It should be able to take a string to be "logged".

### `deprecated`

Marks a function as deprecated. Whenever the function is call it will emit a deprecation warning.

### `timeit`

Emits how long the function took to complete.

**Arguments**

* `report_func` (default `print`) - Callable that the debugging message will be sent to. This could be a logging function (i.e. `logger.error`). It should be able to take a string to be "logged".

## Django specific decorators

### `admin_action`

Identifies a method as an admin action on a Django admin model. Sets the short description of the action to whatever the methods doc string is. This just makes custom admin actions a little cleaner.

## Miscellaneous 

### `skip`

Marks a function to be skipped. When the function is called it will not actually be called. Replaces the function with a function that always returns None.

### `skip_if_env`

Marks a function to be skipped. When the function is called it will not actually be called if a specified environment variable is set. Replaces the function with a function that always returns None.

**Arguments**

* `envar` (default `'SKIP'`) - The environment variable to be checked to determine if the function should actually be called or if a None should just be returned.

### `threaded`

Turns a function into a threaded function. Every time the function is called it's execution in a thread. This functions return type is changed to be the thread being executed. Results are stored in the returned threads `result` attribute.

Example usage:

```python
from ornamentation import threaded

@threaded
def my_func():
    return 100 + 1

rtn = my_func()
print(rtn.result.get())  # this will wait for the thread to complete and return the result.
```

This decorator was adapted from http://stackoverflow.com/a/14331755/18992 (thanks bj0).

**Arguments**

* `daemon` (default `False`) - Should the thread be run in daemon mode.
