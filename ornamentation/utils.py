
def one_and_only_one(items):
    """Returns whether or not only one item is truthy in the iterable."""
    i = iter(items)
    return any(i) and not any(i)
