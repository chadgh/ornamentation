# ornamentation

A collection of, hopefully useful, decorators.

See the [documentation](docs/index.md) for more information.

## Contributing to this project

### Project development setup

Follow these steps to setup your development environment:

1. Clone the repository. Run `git clone https://gitlab.com/chadgh/ornamentation.git`.
2. From within the newly created repository directory, create a virtualenv. Run `cd ornamentation && python -m venv venv` (or create a virtualenv in your favorite way).
3. Activate virtualenv. Run `source venv/bin/activate` (or activate the virtualenv in the way that you like activating virtualenvs).
4. Install dependencies. Run `pip install -r dev_requirements.txt`.
5. Setup pre-commit. Run `pre-commit install`.
6. Contribute!

### Testing in this project

Running `pytest tests.py` will run the tests.

The tests can also be run using `tox`. Simply run `tox` on the command line and the tests should run for all supported versions of Python.

Merge requests should come with tests to make sure that a bug is fixed or that new functionality functions as expected. The CI tests will fail if branch coverage is below 100%.
